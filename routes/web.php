<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('orders', ['orders' => \App\Models\Order::all()]);
});

Route::get('orders/{order}/confirmation', function (\App\Models\Order $order) {
    return response()->pdf(
        view('orders.confirmation', compact('order'))
    );
})->name('order-confirmation');
