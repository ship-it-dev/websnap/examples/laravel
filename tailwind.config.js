const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    theme: {
        fontFamily: {
            sans: ['Nunito', ...defaultTheme.fontFamily.sans],
        },
        extend: {
            colors: {
                primary: {
                    500: '#4fd1c5',
                    700: '#39998f',
                }
            }
        },
    },
    plugins: [],
}
