<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{config('app.name')}}</title>

    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

</head>
<body class="antialiased h-screen bg-gray-100">
    <header class="p-3 sm:px-6 lg:px-8 bg-white">
        <a href="{{url('')}}">
            <x-logo/>
        </a>
    </header>
    <div class="bg-gray-100 py-4">

        <div class="px-3 sm:px-6 lg:px-8 max-w-6xl mx-auto">
            <div class="mt-4 md:mt-8 bg-white rounded p-3 md:p-6 grid grid-cols-3 md:grid-cols-4 gap-3 items-center">
                <span class="font-bold md:col-span-2">{{__('Order #')}}</span>
                <span class="font-bold text-center">{{__('Created at')}}</span>
                <span></span>

                @foreach($orders as $order)
                    <span class="md:col-span-2">#{{$order->increment_id}}</span>
                    <span class="text-center">{{$order->created_at}}</span>
                    <span class="text-right">
                        <a
                            class="inline-block p-2 bg-primary-500 hover:bg-primary-700 text-white rounded"
                            href="{{route('order-confirmation', $order)}}"
                            download="Order #{{$order->increment_id}} - Confirmation"
                        >
                            {{__('Download Confirmation')}}
                        </a>
                    </span>
                @endforeach

            </div>
        </div>
    </div>
</body>
</html>
