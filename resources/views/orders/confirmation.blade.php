<?php
/**
 * @var \App\Models\Order $order
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Confirmation - {{$order->increment_id}}</title>

    <style>{!! file_get_contents(public_path('css/app.css')) !!}</style>

    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
</head>
<body class="text-sm">
    <div class="px-12 space-y-8">

        <div class="flex justify-between">
            <div class="pt-32">
                Jane Doe<br>
                Fakestreet 123<br>
                1234 Faketown
            </div>
            <img src="https://www.websnap.app/assets/img/preview-tool/logo.svg" alt="MyFashionStore">
        </div>

        <h1 class="text-xl font-bold">Your order #{{$order->increment_id}}</h1>

        <div class="space-y-1">
            <p>Hello Jane!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At aut commodi consequatur deleniti eos iusto
                maiores molestiae molestias nobis nulla pariatur quia saepe sed, sint suscipit tenetur veritatis, vero
                voluptate.</p>
        </div>

        <div class="grid grid-cols-2 gap-4">
            <div class="space-y-2">
                <h3 class="font-bold">Shipping Address</h3>
                <p>
                    Jane Doe<br>
                    Fakestreet 123<br>
                    1234 Faketown
                </p>
                <p>
                    Phone: +43 123456<br>
                    Email: jane@doe.com
                </p>
            </div>
            <div class="space-y-2">
                <h3 class="font-bold">Billing Address</h3>
                <p>
                    Jane Doe<br>
                    Fakestreet 123<br>
                    1234 Faketown
                </p>
                <p>
                    Phone: +43 123456<br>
                    Email: jane@doe.com
                </p>
            </div>
        </div>

        <div class="grid grid-cols-5 gap-y-3 items-center">
            <strong class="pb-2 col-span-2 border-b">Product</strong>
            <strong class="pb-2 text-center border-b">Unit price</strong>
            <strong class="pb-2 text-center border-b">Quantity</strong>
            <strong class="pb-2 text-right border-b">Row total</strong>

            <div class="col-span-2 flex gap-3 items-center">
                <img class="flex-grow-0" src="https://via.placeholder.com/60x60" alt="">
                <div>
                    <strong>Product name</strong>
                    <div class="text-sm">SKU: product-123</div>
                </div>
            </div>
            <div class="text-center">€ 9,99</div>
            <div class="text-center">3</div>
            <div class="text-right">€ 29,97</div>
            <div class="col-span-2 flex gap-3 items-center">
                <img class="flex-grow-0" src="https://via.placeholder.com/60x60" alt="">
                <div>
                    <strong>Product name</strong>
                    <div class="text-sm">SKU: product-123</div>
                </div>
            </div>
            <div class="text-center">€ 9,99</div>
            <div class="text-center">3</div>
            <div class="text-right">€ 29,97</div>
            <div class="col-span-2 flex gap-3 items-center">
                <img class="flex-grow-0" src="https://via.placeholder.com/60x60" alt="">
                <div>
                    <strong>Product name</strong>
                    <div class="text-sm">SKU: product-123</div>
                </div>
            </div>
            <div class="text-center">€ 9,99</div>
            <div class="text-center">3</div>
            <div class="text-right">€ 29,97</div>

            <div class="col-span-4 border-t text-right pt-2">Subtotal</div>
            <div class="border-t text-right pt-2">€ 89,10</div>

            <div class="col-span-4 text-right">Tax - 20%</div>
            <div class="text-right">€ 17,82</div>

            <div class="col-span-4 text-right font-bold border-t border-b-4 py-2">Grand Total</div>
            <div class="text-right font-bold border-t border-b-4 py-2">€ 106,92</div>
        </div>
    </div>
</body>
</html>


