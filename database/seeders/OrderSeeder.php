<?php

namespace Database\Seeders;

use Database\Factories\OrderFactory;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{

    public function run()
    {
        $startValue = 7114751;
        $date = now()->subMonth();

        OrderFactory::times(10)
            ->sequence(fn(Sequence $sequence) => [
                'increment_id' => $startValue + $sequence->index,
                'created_at' => $date->addSeconds(rand(3600, 7200)),
                'updated_at' => $date
            ])
            ->create();
    }
}
